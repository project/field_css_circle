<?php

/**
 * @file
 * Contains field_css_circle.theme.inc.
 */

/**
 * Prepares variables for CSS Circle element template.
 *
 * Default template: css-circle.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - account: Current User object.
 */
function template_preprocess_css_circle(array &$variables) {

}
