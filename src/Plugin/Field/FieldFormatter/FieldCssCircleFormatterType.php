<?php

namespace Drupal\field_css_circle\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'field_css_circle_formatter_type' formatter.
 *
 * @FieldFormatter(
 *   id = "field_css_circle_formatter_type",
 *   label = @Translation("CSS Circle"),
 *   field_types = {
 *     "field_css_circle_field_type"
 *   }
 * )
 */
class FieldCssCircleFormatterType extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'size'  => 'default',
        'color' => 'default',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
        'size'  => [
          '#title'         => $this->t('Size'),
          '#type'          => 'select',
          '#default_value' => $this->getSetting('size'),
          '#options'       => $this->getSizes(),
        ],
        'color' => [
          '#title'         => $this->t('Color'),
          '#type'          => 'select',
          '#default_value' => $this->getSetting('color'),
          '#options'       => $this->getColors(),
        ],
      ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $sizes = $this->getSizes();
    $colors = $this->getColors();

    $summary[] = $this->t('Size: @size', [
      '@size' => $sizes[$this->getSetting('size')],
    ]);

    $summary[] = $this->t('Color: @color', [
      '@color' => $colors[$this->getSetting('color')],
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type'          => 'css_circle',
        '#value'         => $this->viewValue($item),
        '#value_display' => $this->viewValueDisplay($item),
        '#size'          => $this->getSetting('size'),
        '#color'         => $this->getSetting('color'),
      ];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    return (int) $item->value;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValueDisplay(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return Html::escape($item->value_display);
  }

  /**
   * @return array
   */
  protected function getSizes() {
    return [
      'default' => $this->t('Default'),
      'small'   => $this->t('Small'),
      'big'     => $this->t('Big'),
    ];
  }

  /**
   * @return array
   */
  protected function getColors() {
    return [
      'default' => $this->t('Default (Blue)'),
      'green'   => $this->t('Green'),
      'orange'  => $this->t('Orange'),
    ];
  }

}
