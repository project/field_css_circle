<?php

namespace Drupal\field_css_circle\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'field_css_circle_field_type' field type.
 *
 * @FieldType(
 *   id = "field_css_circle_field_type",
 *   label = @Translation("CSS Circle"),
 *   description = @Translation("Field CSS Circle"),
 *   default_widget = "field_css_circle_widget_type",
 *   default_formatter = "field_css_circle_formatter_type"
 * )
 */
class FieldCssCircleFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
        'max_length' => 255,
      ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Percentage'))
      ->setRequired(TRUE);

    $properties['value_display'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Display value'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'value'         => [
          'type'    => 'int',
          'default' => 0,
        ],
        'value_display' => [
          'type'   => 'varchar',
          'length' => (int) $field_definition->getSetting('max_length'),
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    $type_manager = \Drupal::typedDataManager();
    $constraint_manager = $type_manager->getValidationConstraintManager();

    $constraints[] = $constraint_manager->create('ComplexData', [
      'value' => [
        'Range' => [
          'min'        => 0,
          'minMessage' => t('%name: may not be lower than @min characters.', [
            '%name' => $this->getFieldDefinition()->getLabel(),
            '@min'  => 0,
          ]),
          'max'        => 100,
          'maxMessage' => t('%name: may not be higher than @max characters.', [
            '%name' => $this->getFieldDefinition()->getLabel(),
            '@max'  => 100,
          ]),
        ],
      ],
    ]);

    if ($max_length = $this->getSetting('max_length')) {
      $constraints[] = $constraint_manager->create('ComplexData', [
        'value_display' => [
          'Length' => [
            'max'        => $max_length,
            'maxMessage' => t('%name: may not be longer than @max characters.', [
              '%name' => $this->getFieldDefinition()->getLabel(),
              '@max'  => $max_length,
            ]),
          ],
        ],
      ]);
    }

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();
    $values['value'] = mt_rand(0, 100);
    $values['value_display'] = $random->word(mt_rand(1, $field_definition->getSetting('max_length')));
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $elements = [];

    $elements['max_length'] = [
      '#type'          => 'number',
      '#title'         => t('Maximum length for Display value'),
      '#default_value' => $this->getSetting('max_length'),
      '#required'      => TRUE,
      '#description'   => t('The maximum length of the field in characters.'),
      '#min'           => 1,
      '#disabled'      => $has_data,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

}
