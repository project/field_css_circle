<?php

namespace Drupal\field_css_circle\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'field_css_circle_widget_type' widget.
 *
 * @FieldWidget(
 *   id = "field_css_circle_widget_type",
 *   module = "field_css_circle",
 *   label = @Translation("CSS Circle"),
 *   field_types = {
 *     "field_css_circle_field_type"
 *   }
 * )
 */
class FieldCssCircleWidgetType extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'value_size'                => 60,
        'value_placeholder'         => '',
        'value_display_size'        => 60,
        'value_display_placeholder' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['value_size'] = [
      '#type'          => 'number',
      '#title'         => t('Textfield size for Percentage'),
      '#default_value' => $this->getSetting('value_size'),
      '#required'      => TRUE,
      '#min'           => 1,
    ];

    $elements['value_placeholder'] = [
      '#type'          => 'textfield',
      '#title'         => t('Placeholder for Percentage'),
      '#default_value' => $this->getSetting('value_placeholder'),
      '#description'   => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    $elements['value_display_size'] = [
      '#type'          => 'number',
      '#title'         => t('Textfield size for Display value'),
      '#default_value' => $this->getSetting('value_display_size'),
      '#required'      => TRUE,
      '#min'           => 1,
    ];

    $elements['value_display_placeholder'] = [
      '#type'          => 'textfield',
      '#title'         => t('Placeholder for Display value'),
      '#default_value' => $this->getSetting('value_display_placeholder'),
      '#description'   => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = t('Textfield size for Percentage: @size', ['@size' => $this->getSetting('value_size')]);
    if (!empty($this->getSetting('value_placeholder'))) {
      $summary[] = t('Placeholder for Percentage: @placeholder', ['@placeholder' => $this->getSetting('value_placeholder')]);
    }

    $summary[] = t('Textfield size for Display value: @size', ['@size' => $this->getSetting('value_display_size')]);
    if (!empty($this->getSetting('value_display_placeholder'))) {
      $summary[] = t('Placeholder for Display value: @placeholder', ['@placeholder' => $this->getSetting('value_display_placeholder')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
      '#type'          => 'number',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
      '#size'          => $this->getSetting('value_size'),
      '#placeholder'   => $this->getSetting('value_placeholder'),
      '#min'           => 0,
      '#max'           => 100,
    ];

    $element['value_display'] = [
      '#type'          => 'textfield',
      '#default_value' => isset($items[$delta]->value_display) ? $items[$delta]->value_display : NULL,
      '#size'          => $this->getSetting('value_display_size'),
      '#placeholder'   => $this->getSetting('value_display_placeholder'),
      '#maxlength'     => $this->getFieldSetting('max_length'),
    ];

    return $element;
  }

}
