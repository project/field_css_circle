<?php

namespace Drupal\field_css_circle\Element;

use Drupal\Core\Render\Element\FormElement;

/**
 * Provides a Css Circle form element.
 *
 * @FormElement("css_circle")
 */
class CssCircle extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);

    return [
      '#pre_render' => [[$class, 'preRenderCssCircle']],
      '#theme'      => 'css_circle',
    ];
  }

  /**
   * Render element for css-circle.html.twig.
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #value, #value_display, #size, #color
   *
   * @return array
   *   The $element with prepared variables ready for css-circle.html.twig.
   */
  public static function preRenderCssCircle(array $element) {
    if (!isset($element['value'])) {
      $element['value'] = 0;
    }

    if (!isset($element['value_display'])) {
      $element['value_display'] = '';
    }

    if (!isset($element['size'])) {
      $element['size'] = 'default';
    }

    if (!isset($element['color'])) {
      $element['color'] = 'default';
    }

    // Attach library.
    $element['#attached']['library'][] = 'field_css_circle/css_circle';

    return $element;
  }

}
